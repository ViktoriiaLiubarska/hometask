package models;

import lombok.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StoreRequestModel {
    @EqualsAndHashCode.Exclude
    private long id;
    private int petId;
    private int quantity;
    @EqualsAndHashCode.Exclude
    private String shipDate;
    private String status;
    private boolean complete;

}
