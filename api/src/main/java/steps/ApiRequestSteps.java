package steps;


import io.qameta.allure.Step;
import io.restassured.response.Response;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class ApiRequestSteps {

    private static Response response;

    public static Response getResponse(){
        return response;
    }

    @Step("POST request object")
    public static Response postRequestObject(String path, Object object){
        return response=given()
                .spec(ApiRequestSpecificationSteps.getRequestSpecification(path))
                .body(object)
                .when()
                .post();
    }
    @Step("GET request with parameters")
    public static Response getRequestWithParams(String path, Map<String,String> params){
        return response=given()
                .spec(ApiRequestSpecificationSteps.getRequestSpecification(path))
                .params(params)
                .when()
                .get();
    }

    @Step("GET request without parameters")
    public static Response getRequest(String path){
        return response=given()
                .spec(ApiRequestSpecificationSteps.getRequestSpecification(path))
                .when()
                .get();
    }

    @Step("DELETE request")
    public static Response deleteRequest(String path){
        return response=given()
                .spec(ApiRequestSpecificationSteps.getRequestSpecification(path))
                .when()
                .delete();
    }
}
