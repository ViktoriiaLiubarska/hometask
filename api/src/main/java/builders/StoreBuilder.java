package builders;

import models.StoreRequestModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTime;


public class StoreBuilder {
    public static StoreRequestModel storeBuilder(int petId){
        return StoreRequestModel.builder()
                .petId(petId)
                .quantity(Integer.parseInt(RandomStringUtils.randomNumeric(2)))
                .shipDate(DateTime.now().toString())
                .status("placed")
                .complete(true)
                .build();
    }
}
