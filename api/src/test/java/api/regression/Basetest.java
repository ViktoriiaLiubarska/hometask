package api.regression;

import builders.StoreBuilder;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import models.StoreRequestModel;
import org.testng.Assert;
import org.testng.annotations.Test;
import steps.ApiRequestSteps;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

@Epic("Petstore")
@Feature("Endpoint store")
@Story("Positive tests for store")
public class Basetest {
    private static final String PATH = "https://petstore.swagger.io/v2/store/order";
    private static final String PATH_GET = "https://petstore.swagger.io/v2/store/inventory";
    private static final String PATH_GET_WITH_PARAMS = "https://petstore.swagger.io/v2/store/order";
    private StoreRequestModel storeRequestModel = StoreBuilder.storeBuilder(5);
    private long id;

    @Test(description = "Create new order")
    public void createNewOrder() {
        ApiRequestSteps.postRequestObject(PATH, storeRequestModel);
        Assert.assertEquals(ApiRequestSteps.getResponse().getStatusCode(), 200);
        id = ApiRequestSteps.getResponse().jsonPath().get("id");
        Assert.assertEquals(ApiRequestSteps.getResponse().as(StoreRequestModel.class), storeRequestModel);
    }

    @Test(description = "Search inventory")
    public void searchInventory() {
        ApiRequestSteps.getRequest(PATH_GET);
        Assert.assertEquals(ApiRequestSteps.getResponse().getStatusCode(), 200);
    }

    @Test(description = "Search newly created order", dependsOnMethods = "createNewOrder")
    public void searchNewlyCreatedOrder() {
        ApiRequestSteps.getRequest(PATH_GET_WITH_PARAMS + "/" + id);
        Assert.assertEquals(ApiRequestSteps.getResponse().getStatusCode(), 200);
        Assert.assertEquals(ApiRequestSteps.getResponse().as(StoreRequestModel.class), storeRequestModel);
    }

    @Test(description = "Delete newly created order", dependsOnMethods = "searchNewlyCreatedOrder")
    public void deleteNewlyCreatedOrder() {
        ApiRequestSteps.deleteRequest(PATH_GET_WITH_PARAMS + "/" + id);
        Assert.assertEquals(ApiRequestSteps.getResponse().getStatusCode(), 200);
    }

    @Test(description = "Search deleted order", dependsOnMethods = "deleteNewlyCreatedOrder")
    public void searchDeletedOrder() {
        ApiRequestSteps.getRequest(PATH_GET_WITH_PARAMS + "/" + id);
        Assert.assertEquals(ApiRequestSteps.getResponse().getStatusCode(), 404);
    }
}
