package steps;

import io.cucumber.java.en.*;

import static org.junit.Assert.*;
import static pages.Constants.*;

public class VerifyAuthorizationTestSteps {
    @When("User clicks on Sign in button")
    public void user_clicks_on_sign_in_button() {
        HOME_PAGE.clickOnSignInButton();
    }

    @When("User enters his email {string}")
    public void user_enters_his_email(String string) {
        SIGN_IN_PAGE.enterEmail(string);
    }

    @When("User enters his password {string}")
    public void user_enters_his_password(String string) {
        SIGN_IN_PAGE.enterPassword(string);
    }

    @Then("Message {string} appears at the top right corner on homepage")
    public void message_appears_at_the_top_right_corner_on_homepage(String string) {
        assertEquals(HOME_PAGE.getHelloMessage(), string);
    }

    @When("User leaves email field empty")
    public void user_leaves_email_field_empty() {
        SIGN_IN_PAGE.enterEmail("");
    }

    @Then("Message Enter your email appears")
    public void message_enter_your_email_appears() {
        assertTrue(SIGN_IN_PAGE.enterEmailMessage().isDisplayed());
    }

    @When("User enters email {string}")
    public void user_enters_email(String string) {
        SIGN_IN_PAGE.enterEmail(string);
    }

    @Then("Error message appears")
    public void error_message_appears() {
        assertTrue(SIGN_IN_PAGE.errorMessage().isDisplayed());
    }

}
