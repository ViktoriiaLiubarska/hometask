package steps;

import io.cucumber.java.en.*;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;
import static pages.Constants.*;

public class AddToCartTestSteps {

    @When("User searches for {string} using search bar")
    public void user_searches_for_using_search_bar(String string) {
        HOME_PAGE.searchByKeyword("huawei p30");
    }

    @When("User clicks on first item on search result page")
    public void user_clicks_on_first_item_on_search_result_page() {
        SEARCH_RESULT_PAGE.clickOnFirstItem();
    }

    @When("User add product to cart")
    public void user_add_product_to_cart() {
        PRODUCT_PAGE.clickOnAddToCartButton();
    }

    @Then("Expected amount of products in cart should be {string}")
    public void expected_amount_of_products_in_cart_should_be(String string) {
        assertEquals(HOME_PAGE.getAmountOfProductsIncart(), string);
    }

}
