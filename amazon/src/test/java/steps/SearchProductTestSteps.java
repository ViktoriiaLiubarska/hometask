package steps;

import io.cucumber.java.en.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static pages.Constants.*;
import static utils.Webdriver.getDriver;


public class SearchProductTestSteps {
    @Given("User is on homepage")
    public void user_is_on_homepage() {
    }

    @When("User searches for a {string} using search bar")
    public void user_searches_for_a_using_search_bar(String string) {
        HOME_PAGE.searchByKeyword(string);
    }

    @Then("URL should contains {string}")
    public void url_should_contains(String string) {
        assertTrue(getDriver().getCurrentUrl().contains(string));
    }

    @Then("Some elements on search page should contain {string}")
    public void some_of_elements_on_search_page_should_contain(String string) {
        assertTrue(SEARCH_RESULT_PAGE.countSearchResultsThatContainsSearchWord(string) > 0);
    }

    @Then("All elements on search page should contain {string}")
    public void all_elements_on_search_page_should_contain(String string) {
        assertEquals(SEARCH_RESULT_PAGE.countSearchResultsThatContainsSearchWord(string), SEARCH_RESULT_PAGE.getActualElementsSize());
    }
}
