import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;


// @RunWith(Cucumber.class)
@RunWith(CucumberWithSerenity.class)
  @CucumberOptions(
          plugin = {"pretty"},
        // glue = "src/main/java/steps/",
          features = "src/test/resources/features"

  )
public class TestRunner {
  }

