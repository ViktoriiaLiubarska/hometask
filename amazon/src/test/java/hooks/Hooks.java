package hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import pages.HomePage;
import pages.ProductPage;
import pages.SearchResultPage;
import pages.SignInPage;

import static pages.Constants.*;
import static utils.Webdriver.getDriver;
import static utils.Webdriver.quit;

public class Hooks {

    private WebDriver driver;

    @Before
    public void setupDriver() {
        driver = getDriver();
        driver.get("https://www.amazon.com/");
        HOME_PAGE = new HomePage();
        PRODUCT_PAGE = new ProductPage();
        SEARCH_RESULT_PAGE = new SearchResultPage();
        SIGN_IN_PAGE = new SignInPage();
    }

    @After
    public void quitDriver() {
        quit();
    }

}
