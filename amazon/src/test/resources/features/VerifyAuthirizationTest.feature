Feature: As a user I want to be able to login to my amazon account

  Background:
    Given User is on homepage
    When User clicks on Sign in button

  Scenario: Authorization with valid credentials
    When User enters his email "daveglis33@gmail.com"
    And User enters his password "amazon"
    Then Message "Hello, Viktoriia" appears at the top right corner on homepage

  Scenario: Authorization with empty email field
    When User leaves email field empty
    Then Message Enter your email appears

  Scenario: Authorization with invalid email
    When User enters email "birds"
    Then Error message appears