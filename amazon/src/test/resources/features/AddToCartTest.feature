Feature: As a user I want to add product to shopping cart

  Scenario: User can add product to cart
    Given User is on homepage
    When User searches for "huawei p30" using search bar
    And User clicks on first item on search result page
    And User add product to cart
    Then Expected amount of products in cart should be "1"