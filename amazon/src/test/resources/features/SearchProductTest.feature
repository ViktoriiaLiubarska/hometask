Feature: As a user I want to be able to find a needed product

  Scenario Outline: A user can find a needed product, some search results contains search product
    Given User is on homepage
    When User searches for a <needed product> using search bar
    Then URL should contains <search keyword>
    And Some elements on search page should contain <needed product>
    Examples:
      | needed product | search keyword   |
      | "huawei p30"   | "s?k=huawei+p30" |

  Scenario Outline: All search results contains search product
    Given User is on homepage
    When User searches for a <needed product> using search bar
    Then All elements on search page should contain <needed product>
    Examples:
      | needed product |
      | "huawei p30"   |

