package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Utils;

import static utils.Webdriver.getDriver;

public class ProductPage extends BasePage {

    @FindBy(xpath = "//input[@id='add-to-cart-button']")
    private WebElement addToCartButton;

    public ProductPage() {
        super();
    }

    public void clickOnAddToCartButton() {
        Utils.waitForPageLoadComplete(getDriver(), 30);
        addToCartButton.click();
    }

}
