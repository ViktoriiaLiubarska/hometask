package pages;

public class Constants {
    public static HomePage HOME_PAGE;
    public static ProductPage PRODUCT_PAGE;
    public static SearchResultPage SEARCH_RESULT_PAGE;
    public static SignInPage SIGN_IN_PAGE;
}
