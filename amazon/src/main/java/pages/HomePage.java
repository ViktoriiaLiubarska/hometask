package pages;

import org.openqa.selenium.Keys;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Utils;

import static utils.Webdriver.getDriver;

public class HomePage extends BasePage {

    @FindBy(xpath = "//input[@id='twotabsearchtextbox']")
    private WebElement searchInput;

    @FindBy(xpath = "//span[@id='nav-cart-count']")
    private WebElement amountOfProductsInCart;

    @FindBy(xpath = "//a[@id='nav-link-accountList']")
    private WebElement signInButton;

    @FindBy(xpath = "//a[@id='nav-link-accountList']//span[@class='nav-line-1']")
    private WebElement helloMessage;

    public HomePage() {
        super();
    }

    public void searchByKeyword(final String keyword) {
        searchInput.sendKeys(keyword, Keys.ENTER);
        Utils.waitForPageLoadComplete(getDriver(), 40);
    }

    public String getHelloMessage() {
        return helloMessage.getText();
    }

    public String getAmountOfProductsIncart() {
        Utils.waitVisibilityOfElement(getDriver(), 30, amountOfProductsInCart);
        return amountOfProductsInCart.getText();
    }

    public void clickOnSignInButton() {
        signInButton.click();
        Utils.waitForPageLoadComplete(getDriver(), 30);
    }

}
