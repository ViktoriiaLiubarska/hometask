package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Utils;
import static utils.Webdriver.getDriver;

import java.util.List;

public class SearchResultPage extends BasePage {

    @FindBy(xpath = "//span/div[contains(@class,'cf-section')]//h2//span")
    private List<WebElement> searchResultsProductsListText;

    public SearchResultPage() {
        super();
    }

    public List<WebElement> getSearchResultsProductsList() {
        return searchResultsProductsListText;
    }

    public int countSearchResultsThatContainsSearchWord(String searchWord) {
        int count = 0;
        for (WebElement webElement : searchResultsProductsListText) {
            if (webElement.getText().toLowerCase().contains(searchWord.toLowerCase())) {
                count++;
            }
        }
        return count;
    }

    public void clickOnFirstItem() {
        searchResultsProductsListText.get(0).click();
    }

    public int getActualElementsSize(){
        return getSearchResultsProductsList().size();
    }
}
