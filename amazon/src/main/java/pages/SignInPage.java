package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

    @FindBy(xpath = "//input[@id='ap_email']")
    private WebElement userEmail;

    @FindBy(xpath = "//input[@id='ap_password']")
    private WebElement userPassword;

    @FindBy(xpath = "//div[@id='auth-email-missing-alert']")
    private WebElement enterEmailMessage;

    @FindBy(xpath = "//div[@id='auth-error-message-box']")
    private WebElement errorMessage;

    public SignInPage() {
        super();
    }

    public SignInPage enterEmail(final String credentials) {
        userEmail.sendKeys(credentials, Keys.ENTER);
        return this;
    }

    public void enterPassword(final String credentials) {
        userPassword.sendKeys(credentials, Keys.ENTER);
    }

    public WebElement errorMessage() {
        return errorMessage;
    }

    public WebElement enterEmailMessage() {
        return enterEmailMessage;
    }
}

